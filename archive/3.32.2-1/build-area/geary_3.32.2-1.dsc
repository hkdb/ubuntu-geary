-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: geary
Binary: geary
Architecture: any
Version: 3.32.2-1
Maintainer: hkdb <hkdb@3df.io>
Homepage: https://wiki.gnome.org/Apps/Geary
Standards-Version: 4.1.3
Build-Depends: debhelper (>= 11), desktop-file-utils, iso-codes, itstool, libcanberra-dev, libenchant-dev, libgcr-3-dev, libgee-0.8-dev, libgirepository1.0-dev, libglib2.0-dev (>= 2.54.0), libgmime-2.6-dev (>= 2.6.17), libgoa-1.0-dev, libgtk-3-dev (>= 3.22.26), libjson-glib-dev, libmessaging-menu-dev, libnotify-dev, libsecret-1-dev, libsoup2.4-dev, libsqlite3-dev (>= 3.12), libunity-dev, libunwind-dev, libwebkit2gtk-4.0-dev (>= 2.20), libxml2-dev, meson (>= 0.41), valac (>= 0.38), libfolks-dev
Package-List:
 geary deb mail optional arch=any
Checksums-Sha1:
 8dba79a25bdb9aa6041aa05e3b76c058da53d5da 3173263 geary_3.32.2.orig.tar.gz
 f2a77105430dad8065fd7e737fe3de685bca1ad1 10192 geary_3.32.2-1.debian.tar.xz
Checksums-Sha256:
 a7fec765e59bc65057e86aba4c36c245e5277ca291b8955b32c61249e6423e03 3173263 geary_3.32.2.orig.tar.gz
 00a25cfce16c02d0d8f075ffba5e447296513b0d38c27aac2dd1e353cc758933 10192 geary_3.32.2-1.debian.tar.xz
Files:
 7ff58a94530d863653fb5c5744971ed6 3173263 geary_3.32.2.orig.tar.gz
 7f7ac3731b312e0e1efe69387c1a2462 10192 geary_3.32.2-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQHABAEBCAAqFiEEIFzKxlFUAtSx+Do2IphPmg6lIDYFAl2KjEAMHGhrZGJAM2Rm
LmlvAAoJECKYT5oOpSA25XwL/A/VE5uidYkOFA5fz5Avh/QyHG2rJbxX8ibXGAnE
bNqQRFgA/EESlN4cQ887VjRJFLK1XzGZ6mDLrNqszUddT/UiAi6TD7w94kRrRpnB
YON3eHAGMICn8yACIIqYXMju37hc6dQ3o7jPh7TQdAMpWhKUWy47X03DabVXt22J
5VZk2HS2hJ4oFoFXiE2btpMhgLbIdNwbV6afOGCfxNQXpJ4o8HEEQXbVXACD/5bI
RJWMb6f3auDrk/x0ZS8RBJzFwRi6wh+OVEg0lvRmZJJZQoqJTlPWD2jR0UgbEzmy
4HFSp5HfTLCSzdAsswzJeSjjcML6aogFPlKPZTSC0thayUv0laoYzNI4H7y6wck2
0dkYkTjAkxpBKWJv7vMOYyXSB1UTwvhpgTE/f3yHTcvPsRSngMrS2hc40g18yxzM
l5Rd2PUR2NnOWLJ+o1Cx+5tNZyIS/ZpPlF+FDCfOBM5TRouRXN1/dfxIpPcpZF4G
xDy4Rxw/DX0qhjDuOe9IXWmufg==
=7fgn
-----END PGP SIGNATURE-----
