#!/bin/bash

#######################################################
#                                                     #
#                       ppbuild                       #
#                                                     #
#           Maintianed by; hkdb <hkdb@3df.io>         #
#                                                     #
#######################################################

NAME="Pull and Prepare Build"
VERSION="v0.04"

HD='/home/hkdb'
POSITIONAL=()
PROJECT='geary'
PPA='hkdb/geary'
SRCDIR="$HD/Development/geary-release"
BDIR="$HD/Development/ubuntu-geary"
GITPATH='git@gitlab.gnome.org:GNOME/geary.git'
LOG='logs/ppbuild.log'
DATE=$(date)
LINE="------------------------------------------------------------"
EMAIL="hkdb@3df.io"

if [ "$#" -lt 6 ] && ["$#" -gt 6] && [ "$1" != "-h" ] && [ "$1" != "--help" ] && [ "$1" != "-v" ] && [ "$1" != "--version" ]; then
    echo -e '\nSomething is missing... Type "./ppbuild -h" without the quotes to find out more...\n'
    exit 0
fi

while [[ $# > 0 ]]
do
key="$1"

case $key in
    -b|--branch)
    BRANCH="$2"
    shift # past argument
    shift # past value
    ;;
    -r|--release)
    REL="$2"
    shift # past argument
    shift # past value
    ;;
    -o|--osrelease)
    OS="$2"
    shift # past argument
    shift # past value
    ;;
    -v|--version)
    echo -e "$VERSION"
    exit 0
    ;;
    -h|--help)
    echo -e "\nThis is a script to automatically pull from the Geary repo and prepare the specified version for building release debs\n\n-v, --version: Release version of this script\n-h, --help: Help/About of this script\n"
    exit 0
    ;;
    *)
    echo -e '\nSomething is missing... Type "./ppbuild -h" without the quotes to find out more...\n'
    exit 0;
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

# Check for Old Logs and Back Them Up if They Exist 
echo -e "\nChecking if old logs exists...\n"
if [ -f "$LOG" ]; then
    echo -e "\nold logs exists... backing up..." >> $LOG
    mv $LOG $LOG.$(date +"%m%d%y")
fi

# Begin Logging
echo -e "\n" >> $LOG
echo -e $LINE >> $LOG
echo -e "$NAME: $DATE" >> $LOG
echo -e $LINE >> $LOG

# Check for git dir and clone if it doesn't exist
if [ ! -d "$SRCDIR/$PROJECT" ]; then
    git -C $SRCDIR clone $GITPATH
fi

# Update Repo
echo -e "\nUpdating GIT Repo...\n" >> $LOG
git -C $SRCDIR/$PROJECT/ pull >> $LOG
echo -e "\nChecking out branch: $BRANCH...\n" >> $LOG
git -C $SRCDIR/$PROJECT/ checkout $BRANCH >> $LOG
echo -e "\nDeleting all GIT data from directory...\n" >> $LOG
rm -rvf $SRCDIR/$PROJECT/.gi* >> $LOG
echo -e "\nRenaming directory, $PROJECT to $PROJECT-$REL...\n" >> $LOG
mv $SRCDIR/$PROJECT $SRCDIR/$PROJECT-$REL >> $LOG
echo -e "\nCreating build directory archive...\n" >> $LOG
tar -czvf $SRCDIR/$PROJECT-$REL.tar.gz -C $SRCDIR $PROJECT-$REL >> $LOG
echo -e "\nMaking build parent directory...\n" >> $LOG
# Check if directory exists or not
DIR=($(ls $BDIR |grep $REL |grep $OS))
echo -e "\nExisting Releases: \n" >> $LOG
DIRSIZE=${#DIR[@]}
echo -e "DIRSIZE: $DIRSIZE" >> $LOG
if [ $DIRSIZE -gt 0 ]; then
    UREL=$((DIRSIZE+1))
    echo -e "UREL: $UREL" >> $LOG
    mkdir -p $BDIR/$REL-$OS-$UREL >> $LOG
    RELDIR="$BDIR/$REL-$OS-$UREL"
else
    mkdir -p $BDIR/$REL-$OS-1
    RELDIR="$BDIR/$REL-$OS-1"
fi
echo -e "\nMoving build directory to build parent directory...\n" >> $LOG
mv $SRCDIR/$PROJECT-$REL $RELDIR >> $LOG
echo -e "\nMoving build dir archive to build parent directory...\n" >> $LOG
mv $SRCDIR/$PROJECT-$REL.tar.gz $RELDIR >> $LOG
echo -e "\nRelease prepared at: $RELDIR\n\n" >> $LOG
echo -e "\nRelease prepared at: $RELDIR\n\n"

# End Logging
echo -e "\n" >> $LOG
echo -e $LINE >> $LOG
echo -e "$NAME: $(date)" >> $LOG
echo -e "$LINE\n\n" >> $LOG

echo -e "\nMoving ppbuild.log to ppbuild.log.$(date +"%m%d%y")\n\n" >> $LOG
mv $LOG $LOG.$(date +"%m%d%y")
