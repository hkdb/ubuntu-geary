-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: geary
Binary: geary
Architecture: any
Version: 3.34.1-disco-1
Maintainer: hkdb <hkdb@3df.io>
Homepage: https://wiki.gnome.org/Apps/Geary
Standards-Version: 4.1.3
Build-Depends: debhelper (>= 11), desktop-file-utils, iso-codes, itstool, libcanberra-dev, libenchant-dev, libgcr-3-dev, libgee-0.8-dev, libgirepository1.0-dev, libglib2.0-dev (>= 2.54.0), libgmime-2.6-dev (>= 2.6.17), libgoa-1.0-dev, libgtk-3-dev (>= 3.22.26), libjson-glib-dev, libmessaging-menu-dev, libnotify-dev, libsecret-1-dev, libsoup2.4-dev, libsqlite3-dev (>= 3.12), libunity-dev, libunwind-dev, libwebkit2gtk-4.0-dev (>= 2.20), libxml2-dev, meson (>= 0.41), valac (>= 0.38), libfolks-dev, libappstream-glib-dev, libgspell-1-dev, libytnef0-dev, libhandy-0.0-dev
Package-List:
 geary deb mail optional arch=any
Checksums-Sha1:
 9302572629bf33aa921ea0e17d4325e702bf8a5c 3271633 geary_3.34.1-disco.orig.tar.gz
 8004fac891582faf6291b8bfa6608afa25693fc6 10232 geary_3.34.1-disco-1.debian.tar.xz
Checksums-Sha256:
 9c14bc2dd8ecf86988b3694a77df3b2cb8af580b05598841ca4f5767fe3e6bd2 3271633 geary_3.34.1-disco.orig.tar.gz
 0cd033e18fcafd2f5ed48446cf0f5aa57e34ad265820232ad68b13e1e6e935b1 10232 geary_3.34.1-disco-1.debian.tar.xz
Files:
 48867fd308bebe54b6aab681ed0434d9 3271633 geary_3.34.1-disco.orig.tar.gz
 40a5c8329df3dfafc968e67153c326e4 10232 geary_3.34.1-disco-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQHABAEBCAAqFiEEIFzKxlFUAtSx+Do2IphPmg6lIDYFAl2cdNcMHGhrZGJAM2Rm
LmlvAAoJECKYT5oOpSA2oWkL/3hJl0rmUIX622z4vw2PVB9ZzZaztZyfrpX/nyv5
k873CL+W/azJF401owc0NXx8MimgnQpjDIqyjXllLesFKDFJifgPlxIrpW7nOvRq
l+Dds2X/bOd2aFMnlIs9ZAGExL3RlBwK8N1chrcuBSURUcPvL3WXmqxxmwJLCOkC
go976vGyr+f4qMHqR8iLbpwId14Fe+Hbo5TsoKiN46QKN/GLuMX7qCRNffeWVX2l
jrw074z7+0FTxpdV1fQbLC6WLuSIbN4C6Ag9Ei5dHr5oLNszIBXeO2kcArVtHcB9
oL843CgdqrqKj+PILg/HwZVUhcNNB1wW15q6T2y9HBe5aYO2KdoOfogEzQohJSGl
fhxE3j6P5vQHytUw4mg7nL94rWhrDLSy7awoQ0Pa1zh5icOnKy1WEZJMiSQmRZbp
L+MZRuAwi2xxtL71PKdcAomQqsrO4A+k16BCrRFDifL+9n34zrQNlOFgLBbK6e1e
kcdeDZhHfB4sXwYd3alEBoYXfg==
=5pjS
-----END PGP SIGNATURE-----
