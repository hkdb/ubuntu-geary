-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: geary
Binary: geary
Architecture: any
Version: 3.34.0-disco-1
Maintainer: hkdb <hkdb@3df.io>
Homepage: https://wiki.gnome.org/Apps/Geary
Standards-Version: 4.1.3
Build-Depends: debhelper (>= 11), desktop-file-utils, iso-codes, itstool, libcanberra-dev, libenchant-dev, libgcr-3-dev, libgee-0.8-dev, libgirepository1.0-dev, libglib2.0-dev (>= 2.54.0), libgmime-2.6-dev (>= 2.6.17), libgoa-1.0-dev, libgtk-3-dev (>= 3.22.26), libjson-glib-dev, libmessaging-menu-dev, libnotify-dev, libsecret-1-dev, libsoup2.4-dev, libsqlite3-dev (>= 3.12), libunity-dev, libunwind-dev, libwebkit2gtk-4.0-dev (>= 2.20), libxml2-dev, meson (>= 0.41), valac (>= 0.38), libfolks-dev, libappstream-glib-dev, libgspell-1-dev, libytnef0-dev, libhandy-0.0-dev
Package-List:
 geary deb mail optional arch=any
Checksums-Sha1:
 714f998c7c0cdf3e38ca333e9b5bd0e78a08e973 3272536 geary_3.34.0-disco.orig.tar.gz
 c82a4a56e948fee0a302f082e2aadb31b9a8ca5f 10288 geary_3.34.0-disco-1.debian.tar.xz
Checksums-Sha256:
 20ea7d3c2a821081fed7ea772c4404c1bef9cdd5e719ac826c2245b1f756d591 3272536 geary_3.34.0-disco.orig.tar.gz
 d290e20104542b54d1de678d1d2218914a04738a0b1810902b4312098c3e1ca5 10288 geary_3.34.0-disco-1.debian.tar.xz
Files:
 5c8a1c9aa25a62246841b930fc8d7c01 3272536 geary_3.34.0-disco.orig.tar.gz
 5d3ba9479a7edb9cd276c7965fae2161 10288 geary_3.34.0-disco-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQHABAEBCAAqFiEEIFzKxlFUAtSx+Do2IphPmg6lIDYFAl2OE3YMHGhrZGJAM2Rm
LmlvAAoJECKYT5oOpSA2XrEMAIdcBRy1Lwpb/d3NI1ND7fU08RPMJaq7erBPH/jY
Bm/VIw2PmEhLqEzlgZWn01//v7Z0m9XBnb/i6zOvyrIRIdXJIqZBc8sBM79j/ZNK
z2vy2uNF1SiRsuibD7WqJvjyWdH58cMztRygwj1cwraKhlTID3baa/PaDSG64edA
3p+7LrfJYZnx13yUyf2/SK0H+srGyQAZshSujdH+rcQ5G9CX8dudz87rSvUru6Vt
DLE0NWfBBL1A9glk+dUbGnRGCMe8m1AErRLrCvXJVq4xu8Ye0xpT3HhS81rN+lOa
wzQlFf4zNpjF7wpElE+S0day7v7gx7o0PZHHa0+Ee14qIlBQx94nBdRu3xohMnFF
VDMemghFYiDf8/JRaSkeuMnMpqSosH+f7T1zZHku/cas4NWTcoa+/scf5r+IRuTA
oZ7KxwAqR7CNt1puFo4VQVe4XObAN4chuCT+41y7m7s5pzf/N+B4Lb2cKLLzGmzj
uni81u3HwFm4b+K+baRYMp1mHA==
=RjH3
-----END PGP SIGNATURE-----
