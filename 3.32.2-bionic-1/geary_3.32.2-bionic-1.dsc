-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: geary
Binary: geary
Architecture: any
Version: 3.32.2-bionic-1
Maintainer: hkdb <hkdb@3df.io>
Homepage: https://wiki.gnome.org/Apps/Geary
Standards-Version: 4.1.2
Build-Depends: debhelper (>= 10), desktop-file-utils, iso-codes, itstool, libcanberra-dev, libenchant-dev, libgcr-3-dev, libgee-0.8-dev, libgirepository1.0-dev, libglib2.0-dev (>= 2.54.0), libgmime-2.6-dev (>= 2.6.17), libgoa-1.0-dev, libgtk-3-dev (>= 3.22.26), libjson-glib-dev, libmessaging-menu-dev, libnotify-dev, libsecret-1-dev, libsoup2.4-dev, libsqlite3-dev (>= 3.12), libunity-dev, libunwind-dev, libwebkit2gtk-4.0-dev (>= 2.20), libxml2-dev, meson (>= 0.41), valac (>= 0.38), libfolks-dev
Package-List:
 geary deb mail optional arch=any
Checksums-Sha1:
 965d58f233aeb527a677b72a7e006839efd6fa89 3160841 geary_3.32.2-bionic.orig.tar.gz
 0b8c5ab400030c99fb784bad454a8b8ec223266c 9260 geary_3.32.2-bionic-1.debian.tar.xz
Checksums-Sha256:
 3a67b3a51087768fb59e1c5ed64883c69e7f68b3b78fd1550cfdff2375781c98 3160841 geary_3.32.2-bionic.orig.tar.gz
 cb91c6eabbfc9fe6e83d65258b41e5f0f58a4985b575890679d533129afc9577 9260 geary_3.32.2-bionic-1.debian.tar.xz
Files:
 fd0ed4d62d95c3ebfb7d250345b0a889 3160841 geary_3.32.2-bionic.orig.tar.gz
 97c9459e8656db192c277a49d40379c3 9260 geary_3.32.2-bionic-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQHABAEBCgAqFiEEIFzKxlFUAtSx+Do2IphPmg6lIDYFAl2NCJQMHGhrZGJAM2Rm
LmlvAAoJECKYT5oOpSA2gQ0L/1QOQNmWQkt9BT2fJakeJgWzi00hwlKwJfq06hEF
3Lpj23doTxv+tFp1VeZjSO5YuZw+/5X7A0TiuwVnto4yZlywcEeUcx6iReoiudtr
rHpiakZiWDUFM/nF1JhMZSTq5rrYU91DT3XO0ZpgOuVeq+nZTncpBT7OlWevTE8M
gvcu0RrBSKBCG4TXr5my7fnocI/Lp5/sQDkHJqciy0AZgteuziDCu0XkVOOTCNjS
dlQMGyR0l3400VdDsEwwx9SlDLQVeEmHgq8dp2PmCtK8+C6hCfq4Q2gN/Kd2TI9y
zLeYlF9VkF+WqIx+WqaBCcme5zhReMu7KW7T9Q+3zKshuassZPeAo92fkPnQ4DvW
fV/m+say6rr+Hif29Zs5fa/bh88I9fh2I0hYy5M0JYkemzDIix5ZCmuoq4You+6P
NkOPJ73UlWwF66IJLM4TjG9HIM9DjeqySc0uxUXoRI5jL+dVUInXpQCu2M3P08fQ
cOXxBTVdCC3e7q0tdbCoBfOaIA==
=qMdY
-----END PGP SIGNATURE-----
