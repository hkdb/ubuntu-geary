-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: geary
Binary: geary
Architecture: any
Version: 3.32.2-disco-1
Maintainer: hkdb <hkdb@3df.io>
Homepage: https://wiki.gnome.org/Apps/Geary
Standards-Version: 4.1.3
Build-Depends: debhelper (>= 11), desktop-file-utils, iso-codes, itstool, libcanberra-dev, libenchant-dev, libgcr-3-dev, libgee-0.8-dev, libgirepository1.0-dev, libglib2.0-dev (>= 2.54.0), libgmime-2.6-dev (>= 2.6.17), libgoa-1.0-dev, libgtk-3-dev (>= 3.22.26), libjson-glib-dev, libmessaging-menu-dev, libnotify-dev, libsecret-1-dev, libsoup2.4-dev, libsqlite3-dev (>= 3.12), libunity-dev, libunwind-dev, libwebkit2gtk-4.0-dev (>= 2.20), libxml2-dev, meson (>= 0.41), valac (>= 0.38), libfolks-dev
Package-List:
 geary deb mail optional arch=any
Checksums-Sha1:
 dcdb87c81332cab91129167512c7b12cfe553443 3171712 geary_3.32.2-disco.orig.tar.gz
 0cce05d745a28df4590e5bd8de92943aaa279511 10152 geary_3.32.2-disco-1.debian.tar.xz
Checksums-Sha256:
 5a8760fc6069fa919d02b03d57ad5fb597111af72f8e576f8a5577f1cc0216cb 3171712 geary_3.32.2-disco.orig.tar.gz
 578d69dfdc84d3a81199ab1b1228d1b2e5b0c71d01ba6a26bbdd347a68c3591f 10152 geary_3.32.2-disco-1.debian.tar.xz
Files:
 4b11ffebdde3e4334378a05ce126aeaa 3171712 geary_3.32.2-disco.orig.tar.gz
 393ac753e9667e606ce015177bfe6fe3 10152 geary_3.32.2-disco-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQHABAEBCAAqFiEEIFzKxlFUAtSx+Do2IphPmg6lIDYFAl2NGaUMHGhrZGJAM2Rm
LmlvAAoJECKYT5oOpSA22b4MAJajrLr0UeSqGIkNBTbQNDh9F8OJB3jYLh/16/i4
meMWkLq587KXm6b4/cntBuPdA0WsDAYgnbYw4W2tKA0yKFNKh64ykBzACYOE18rd
UzMFze7x8VXu0zpI7F6AAQuM7uGeZ7jA8/RkFTBxt1owDIYdZKQIiYAdQ17UmDyd
JAnBsbp6uUBRrfPDMrl1J4tfCDCPpVoO05FK+RPNZOCpy4IUfOuaCrcyROCOgL55
KFMI4FygpMeAD+NKf3CV0np0yvYTVpMw+aAmplB5SydKxsEngE3T1f6fA469Vjxv
2QdfLVyWRGde0NEpIPgoPaNffTl18LBJX85GXWtQWFBWZbR9El2enBQ5sixPMSJe
K0pWdKHNt7E1pZyAQ0eKVvbllNeroBvstyO2Ek8klISQR+NNf8xI91yRNt+cPhaa
bnCGtKMP4KrnXT1r4id2KgJK4jC1fTVK2+BizzNy6AAb9oiqEuzzfTDBPWPvw8Yv
heh1hLRuFdRhckhCITCxUHs66g==
=wKf9
-----END PGP SIGNATURE-----
