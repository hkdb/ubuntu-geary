-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: geary
Binary: geary
Architecture: any
Version: 3.34.2-eoan-1
Maintainer: hkdb <hkdb@3df.io>
Homepage: https://wiki.gnome.org/Apps/Geary
Standards-Version: 4.4.0
Build-Depends: debhelper-compat (= 12), desktop-file-utils, iso-codes, itstool, libcanberra-dev, libenchant-dev, libgcr-3-dev, libgee-0.8-dev, libgirepository1.0-dev, libglib2.0-dev (>= 2.54.0), libgmime-2.6-dev (>= 2.6.17), libgoa-1.0-dev, libgtk-3-dev (>= 3.22.26), libjson-glib-dev, libmessaging-menu-dev, libnotify-dev, libsecret-1-dev, libsoup2.4-dev, libsqlite3-dev (>= 3.12), libunity-dev, libunwind-dev, libwebkit2gtk-4.0-dev (>= 2.20), libxml2-dev, meson (>= 0.41), valac (>= 0.38), libfolks-dev, libappstream-glib-dev, libgspell-1-dev, libytnef0-dev, libhandy-0.0-dev
Package-List:
 geary deb mail optional arch=any
Checksums-Sha1:
 65ad956f0fd61a47194637996a6f80ede7a61934 3285340 geary_3.34.2-eoan.orig.tar.gz
 88099da304d21225795efb23d70d7b5109c278a4 10108 geary_3.34.2-eoan-1.debian.tar.xz
Checksums-Sha256:
 d55a03ff7f188f7c7d095af7e4cb86ab17252deea476622f776fb41dfa85171d 3285340 geary_3.34.2-eoan.orig.tar.gz
 9266be0c1c293e88db037ba4b7ed49a68ebe2cbe3c2e713f1c5855c39b887d5e 10108 geary_3.34.2-eoan-1.debian.tar.xz
Files:
 099194820b8d3e4f78d4f61f709c07b6 3285340 geary_3.34.2-eoan.orig.tar.gz
 eb24f18bb875fbe14cc8d30c55b0b097 10108 geary_3.34.2-eoan-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQHABAEBCAAqFiEEIFzKxlFUAtSx+Do2IphPmg6lIDYFAl3de9IMHGhrZGJAM2Rm
LmlvAAoJECKYT5oOpSA2yHoL+weawURBcIhLzmqw8PkiZEMf//5VWjv8PylREmNl
EAXfHHDIylBQmp9OfzqzpM+Sc40R8jjlC6GGJnjadDJfXhLPLBBiazINywZ6L+yl
ZoDSZKXflRbf/YwJUr6D9A6WwR0NPiVeuhvoe0D/C7RGyJjVEBsrCGB87yfRsoUw
GTkjiHn4RjP4/nCchgQic3xUcak8bNf7FZAxu9QacrmYz3LiwyGFiA231/50XzRI
SRVgbKs1/mDN3qTFFkmj4jWJL9ndK9+6jp0qE7hdzL+sstj6Vno94ncvDTduiX8A
Zls1Ach+qloXMDGjqyU5sw5mxkJjM6G+JiImLNwaZH9tpowh/XEmXEV389cAteu2
Yxi+sLNdj6ATXXdfdFtbnYpJx7dlnOakuav0/q33sYbejsq5F7n2q+bDiwQPGkFp
e9/hKLVua6o855zcfg5q5LWz7e2qcbmKOsHj/8DnCjoOxTptNVG7qOaF8yRg20R2
Mxk/vTsFcf75ZQVDRgikGTAo0w==
=dhyM
-----END PGP SIGNATURE-----
