# UBUNTU GEARY - Distributing Geary to Ubuntu via PPA
maintained by: `hkdb <hkdb@3df.io>`

![Screenshot of Geary](GearySS.png)

### SUMMARY:

This is a repo that assists in the efforts of distributing Geary to ppa:hkdb/geary and to also track all the work we do to for it. Eventually, the goal is to fully automate and distribute via CI/CD.

### BUILD PRE-REQUISITES

```
sudo apt install build-essential ubuntu-dev-tools dh-make lintian libunity-dev pbuilder meson build-essential valac \
        itstool libgirepository1.0-dev libmessaging-menu-dev libnotify-dev \
        desktop-file-utils iso-codes gettext libcanberra-dev \
        libappstream-glib-dev libenchant-dev libfolks-dev libgcr-3-dev libgee-0.8-dev \
        libglib2.0-dev libgmime-2.6-dev libgoa-1.0-dev libgspell-1-dev \
        libgtk-3-dev libjson-glib-dev libhandy-0.0-dev libsecret-1-dev \
        libsqlite3-dev libunwind-dev libwebkit2gtk-4.0-dev \
        libxml2-dev libytnef0-dev libfolks-dev libpeas-dev valadoc -y
pbuilder-dist <ubuntu release> create
export DEBFULLNAME="hkdb"
export DEBEMAIL="hkdb@3df.io"
mkdir -p ~/Development/geary-release
```

### BUILDING INSTRUCTIONS/HISTORY:

Steps taken to distribute the first deb:

`Pull and Prepare to build:`
```
cd geary-release
git clone git@gitlab.gnome.org:GNOME/geary.git
cd geary
git checkout gnome-3-32
rm -rf .gi*
cd ..
mv geary/ geary-3.32.2
tar -czvf geary-3.32.2.tar.gz geary-3.32.2
cp geary-3.32.2.tar.gz ../ubuntu-geary/
cd ../
cd geary-release
git clone git@gitlab.gnome.org:GNOME/geary.git
cd geary
git checkout gnome-3-32
rm -rf .gi*
cd ..
mv geary/ geary-3.32.2
tar -czvf geary-3.32.2.tar.gz geary-3.32.2
mkdir -p ../ubuntu-geary/3.32.2-1~bionic
cp geary-3.32.2.tar.gz ../ubuntu-geary/
```

`GOING FORWARD: You can replace the above with ppbuild:`
```
### USAGE: ###

./ppbuild -b <branch> -r <release> -o <ubuntu release>


### EXAMPLE: ###

./ppbuild -b gnome-3-32 -r 3.32.2 -o bionic
```

`Build Steps:`

Record your steps in the `steps` folder and link them below:

- [09/24/2019 - 3.32.2-1 (disco)](steps/3.32.2-1.md)
- [09/27/2019 - 3.32.2-bionic-1](steps/3.32.2-bionic-1.md)
- [09/27/2019 - 3.32.2-disco-1](steps/3.32.2-disco-1.md)
- [09/27/2019 - 3.34.0-disco-1](steps/3.34.0-disco-1.md)
- [10/08/2019 - 3.34.1-disco-1](steps/3.34.1-disco-1.md)
- [11/27/2019 - 3.34.2-eoan-1](steps/3.34.2-eoan-1.md)

### HISTORY:

- 09/24/2019 - v3.32.2-1_amd64 - For Ubuntu Disco Only
- 09/27/2019 - v3.32.2-bionic-1_amd64 - Bionic
- 09/27/2019 - v3.32.2-disco-1_amd64 - Disco
- 09/27/2019 - DELETED v3.32.2-1_amd64
- 09/27/2019 - v3.34.0-disco-1_amd64 - Disco
- 10/08/2019 - v3.34.1-disco-1_amd64 - Disco
- 11/27/2019 - v3.34.2-eoan-1_amd64 - Eoan

### ppbuild REVISION HISTORY:

- 9/26/2019 - v0.01 - Initial commit
- 9/26/2019 - v0.02 - Fixed README, flow from testing, & multi-ubuntu-release handler
- 9/27/2019 - v0.03 - Added move ppbuild.log to ppbuild.log.date when done
- 9/27/2019 - v0.04 - Fixed logging bug and changed dir convention for better workflow

### SPONSOR:

This repository and the efforts to distribute Geary to Ubuntu via PPA is sponsored by 3DF OSI. For more information, please visit https://osi.3df.io
